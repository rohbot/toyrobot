import zmq
import time

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:8888")
print "Listening on port 8888"
i = 0
while True:
	i +=1
	msg = "test " + str(i)	
	print "sending: " + msg
	socket.send(msg)
	time.sleep(1)
