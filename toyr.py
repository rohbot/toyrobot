import sys

class Robot:
	

	def __init__(self):
		self.on_table = False
		self.table_size = 5

	#Takes in X,Y co-ord and Direction to face.
	def place(self,x,y,f):
		
		#check bounds
		if not self.check_bounds(x,y):
			return
		if not self.check_heading(f):
			return
					
		self.x = x
		self.y = y
		self.facing = f
		self.on_table = True

	def move(self):
		if self.facing == "NORTH":
			new_x = self.x
			new_y = self.y + 1
		elif self.facing == "SOUTH":
			new_x = self.x
			new_y = self.y - 1
		elif self.facing == "EAST":
			new_x = self.x + 1
			new_y = self.y 
		elif self.facing == "WEST":
			new_x = self.x - 1
			new_y = self.y 

		if self.check_bounds(new_x, new_y):
			self.x = new_x
			self.y = new_y
			
	#Simulate 90 deg turn left
	def left(self):
		if self.facing == "NORTH":
			self.facing = "WEST"
		elif self.facing == "SOUTH":
			self.facing = "EAST"
		elif self.facing == "EAST":
			self.facing = "NORTH"
		elif self.facing == "WEST":
			self.facing = "SOUTH"
	
	#Simulate 90 deg turn right 	
	def right(self):
		if self.facing == "NORTH":
			self.facing = "EAST"
		elif self.facing == "SOUTH":
			self.facing = "WEST"
		elif self.facing == "EAST":
			self.facing = "SOUTH"
		elif self.facing == "WEST":
			self.facing = "NORTH"

	def report(self):
		print "Output: " +  str(self.x) + "," + str(self.y) + "," + str(self.facing)

	
	# handles one line of input
	def process_input(self,input):		
		try:
			commands = input.split()
			command = commands[0]
			if command == "PLACE":
				params = commands[1].split(',')
				x = int(params[0])
				y = int(params[1])
				f = params[2]
				self.place(x,y,f)
				
			elif not self.on_table:
				print "PLACE Robot on Table First"
			elif command == "MOVE":
				self.move()
			elif command == "LEFT":
				self.left()
			elif command == "RIGHT":
				self.right()
			elif command == "REPORT":
				self.report()
			else:
				print "Invalid Command:2", input

		except:
			#raise
			print "Invalid Command:3", input
	
	def check_bounds(self,x,y):
		if x < 0 or x > (self.table_size - 1):
			return False
		elif y < 0 or y > (self.table_size - 1):
			return False
		else:
			return True
	

	def check_heading(self,f):
		if f == "NORTH":
			return True
		elif f == "SOUTH":
			return True
		elif f == "EAST":
			return True
		elif f == "WEST":
			return True
		else:
			return False

	def get_input(self):
		input = ''
		while input !='exit':

			input = raw_input("Enter Command: ")
	
			if input == "EXIT" or input == "exit":
				print "Goodbye!"
				sys.exit(0)
			self.process_input(input)	

def test():

	robot = Robot()	
	robot.process_input("PLACE 1,1,WEST")
	robot.process_input("MOVE")
	robot.process_input("MOVE")
	robot.process_input("LEFT")
	robot.process_input("MOVE")
	robot.process_input("REPORT")


robot.get_input()


#while True:
#	get_input()
