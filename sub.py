import zmq

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://localhost:8888")
socket.setsockopt(zmq.SUBSCRIBE, "UPDATE")
print "Listening on port 8888"
while True:
	msg = socket.recv()
	print msg

