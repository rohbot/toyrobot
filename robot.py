import sys
import zmq
import redis

REDIS_KEY = "robot"

class Robot:
	

	def __init__(self):
		self.on_table = False
		self.table_size = 5
		self.using_redis = False
		self.send_updates = False 

	#Takes in X,Y co-ord and Direction to face.
	def place(self,x,y,f):
		
		#check bounds
		if not self.check_bounds(x,y):
			return "Out of Bounds"
		if not self.check_heading(f):
			return "Bad Heading: " + f
					
		self.x = x
		self.y = y
		self.facing = f
		self.on_table = True
		self.update_position()
		return self.report()

	def move(self):
		if self.facing == "NORTH":
			new_x = self.x
			new_y = self.y + 1
		elif self.facing == "SOUTH":
			new_x = self.x
			new_y = self.y - 1
		elif self.facing == "EAST":
			new_x = self.x + 1
			new_y = self.y 
		elif self.facing == "WEST":
			new_x = self.x - 1
			new_y = self.y 

		if self.check_bounds(new_x, new_y):
			self.x = new_x
			self.y = new_y
			self.update_position()
		return str(self.x) + "," + str(self.y)
			
	#Simulate 90 deg turn left
	def left(self):
		if self.facing == "NORTH":
			self.facing = "WEST"
		elif self.facing == "SOUTH":
			self.facing = "EAST"
		elif self.facing == "EAST":
			self.facing = "NORTH"
		elif self.facing == "WEST":
			self.facing = "SOUTH"
		self.update_position()
		return self.facing
	
	#Simulate 90 deg turn right 	
	def right(self):
		if self.facing == "NORTH":
			self.facing = "EAST"
		elif self.facing == "SOUTH":
			self.facing = "WEST"
		elif self.facing == "EAST":
			self.facing = "SOUTH"
		elif self.facing == "WEST":
			self.facing = "NORTH"
		self.update_position()
		return self.facing

	def report(self):
		return "Output: " +  str(self.x) + "," + str(self.y) + "," + str(self.facing)
	
	def restore(self):
		print "Fetching previous value from redis"
		self.redis = redis.Redis()
		self.using_redis = True		
				
		input = self.redis.get(REDIS_KEY)
		if input != None:
			print "redis:", self.process_input(input)

		else:
			print "No previous value in redis"
	
	def config_publisher(self):
		context = zmq.Context()
		self.socket = context.socket(zmq.PUB)
		self.socket.bind("tcp://*:8888")
		print "Listening on port 8888"
		self.send_updates = True
	
	def update_position(self):
		msg = str(self.x) + "," + str(self.y) + "," + str(self.facing)
		if self.using_redis:
			self.redis.set(REDIS_KEY, "PLACE " + msg)
		if self.send_updates:
			print "sending update: " + msg
			self.socket.send("UPDATE " + msg)		

	# handles one line of input
	def process_input(self,input):		
		try:
			commands = input.split()
			command = commands[0]
			if command == "PLACE":
				params = commands[1].split(',')
				x = int(params[0])
				y = int(params[1])
				f = params[2]
				return self.place(x,y,f)
				
			elif not self.on_table:
				return "PLACE Robot on Table First"
			elif command == "MOVE":
				return self.move()
			elif command == "LEFT":
				return self.left()
			elif command == "RIGHT":
				return self.right()
			elif command == "REPORT":
				return self.report()
			else:
				return "Invalid Command:2" + input

		except:
			raise
			return "Invalid Command:3 " + input
	
	def check_bounds(self,x,y):
		if x < 0 or x > (self.table_size - 1):
			return False
		elif y < 0 or y > (self.table_size - 1):
			return False
		else:
			return True
	

	def check_heading(self,f):
		if f == "NORTH":
			return True
		elif f == "SOUTH":
			return True
		elif f == "EAST":
			return True
		elif f == "WEST":
			return True
		else:
			return False

	def get_input(self):
		input = ''
		while input !='exit':

			input = raw_input("Enter Command: ")
	
			if input == "EXIT" or input == "exit":
				print "Goodbye!"
				sys.exit(0)
			print self.process_input(input)	

def test():

	robot = Robot()
	robot.restore()	
	robot.config_publisher()

	robot.process_input("PLACE 1,1,WEST")
	robot.process_input("MOVE")
	robot.process_input("MOVE")
	robot.process_input("LEFT")
	robot.process_input("MOVE")
	robot.process_input("REPORT")

	robot.get_input()

def server():

	robot = Robot()
	robot.restore()
	robot.config_publisher()
	context = zmq.Context()
	socket = context.socket(zmq.REP)
	socket.bind("tcp://*:5555")
	while True:
		message = socket.recv()
		print "Received request: ", message	
		output = robot.process_input(message)
		if output is None:
			output = "0"
		socket.send(output) 
server()
#test()


#while True:
#	get_input()
