import zmq
import sys
import time
from random import choice

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:5555")

cmds = ["MOVE","LEFT","RIGHT"]
while True:
		input = choice(cmds)		
		socket.send(input)
		message = socket.recv()
		print "received: ", message	
		time.sleep(1)
