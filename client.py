import zmq
import sys

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:5555")

while True:		
	input = ''
	while input !='exit':

		input = raw_input("Enter Command: ")

		if input == "EXIT" or input == "exit":
			print "Goodbye!"
			sys.exit(0)
		socket.send(input)
		message = socket.recv()
		print "received: ", message	

